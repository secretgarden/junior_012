/**
 * 文字自带边框的实体
 */
ArrowShowTip = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this);
		this.setCascadeOpacityEnabled(true);
		ll.run.st = this;
	},
	init:function(name, delay){
		var content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		content.setColor(cc.color(0, 0, 0));
		content.setAnchorPoint(0, 0.5);
		this.addChild(content,2);
		var mr = 10;
		var rect = content.getBoundingBoxToWorld();
		var middle = new cc.Scale9Sprite(res_run.show_tip_bg);
		middle.width = rect.width + mr * 2;
		middle.height = rect.height + mr * 2;
		middle.setPosition(rect.x + rect.width * 0.5, rect.y + rect.height * 0.5);
		middle.setCapInsets(cc.rect(5, 5, 280, 63));
		this.addChild(middle);
		
		var arrow = new cc.Sprite("#ui/show_tip_arrow.png");
		this.down(middle, arrow);
		arrow.setPositionX(arrow.getPositionX() * 0.5);
		this.addChild(arrow);
		return this;
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);
		}, this));
		this.runAction(seq);
	},
	up:function (standard, target, margin){
		if(!margin){
			margin = 0;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的y + 标准物的高度 * 缩放 * (1-锚点y) + 本身的高度 * 缩放 * 锚点y + 所需间隔
		var y = standard.y + standard.height * standard.getScaleY() * (1-sap.y)  + target.height * ap.y * target.getScaleY() + margin;
		target.setPosition(standard.x, y);
	},	
	down:function (standard, target, margin){
		if(!margin){
			margin = 0;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y - standard.height * standard.getScaleY() * sap.y  - target.height * (1-ap.y) * target.getScaleY() - margin;
		target.setPosition(standard.x, y);
	}
})