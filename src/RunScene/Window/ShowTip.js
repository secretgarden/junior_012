/**
 * 文字自带边框的实体
 */
ShowTip = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this);
		this.setCascadeOpacityEnabled(true);
		ll.run.st = this;
	},
	init:function(name, delay){
		var content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		content.setColor(cc.color(0, 0, 0));
		content.setAnchorPoint(0, 0.5);
		this.addChild(content,2);
		var mr = 10;
		var rect = content.getBoundingBoxToWorld();
		var bg = new cc.Scale9Sprite(res_start.show_tip);
		bg.width = rect.width + mr * 2;
		bg.height = rect.height + mr * 2;
		bg.setPosition(rect.x + rect.width * 0.5, rect.y + rect.height * 0.5);
		this.addChild(bg);
		
		if(delay == null){
			delay = 2;
		}
		if(delay > 0){
			this.scheduleOnce(this.kill, delay);	
		}
		return this;
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);
		}, this));
		this.runAction(seq);
	}
})