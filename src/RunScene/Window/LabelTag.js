/**
 * 标签
 */
LabelTag = cc.Node.extend({
	ctor:function(p, name){
			this._super();
			p.addChild(this);
			this.setCascadeOpacityEnabled(true);
			this.init(name);
		},
		init:function(name){
			this.draw = new cc.DrawNode();
			this.addChild(this.draw);
			var content = new cc.LabelTTF(name, gg.fontName, 12);
			content.setColor(cc.color(0, 0, 0));
			this.addChild(content);
			var rect = content.getBoundingBox();
			var mr = 3;
			this.draw.drawRect(cc.p(rect.x - mr,rect.y - mr),
					cc.p(rect.x + rect.width + mr,rect.y + rect.height + mr),
					cc.color(200,200,200,150), 2, cc.color(0,200,0,150));
		}
	})