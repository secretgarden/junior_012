var RunMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        ll.run = null;
        ll.main = this;
        this.loadTip();
        this.loadTool();
        this.loadFlow();
        this.loadRun();
        return true;
    },
    loadTip:function(){
    	ll.tip = new TipLayer(this);
    },
    loadTool:function(){
    	ll.tool = new ToolLayer(this);
    },
    loadRun:function(){
    	ll.run = new RunLayer1(this);
    	gg.flow.start();
    },
    loadFlow:function(){
    	gg.flow.setMain(this);
    },
    over: function (){
    	ll.tip.over();
    	this.scheduleOnce(function(){
    		gg.synch_l = false;
    		$.runScene(new FinishScene());
    	},2);
    	// 提交成绩
    	net.saveScore();
    },
});
