var TipLayer = cc.Layer.extend({
	scene:null,
	tip:null,
	time:null,
	score:null,
	flash:null,
	tip_frame:null,
	up_button:null,
	down_button:null,
	rotateAction:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 30);
		this.init();
	},
	init: function (){
		// 引导标记
		this.arr = new Arrow(this);
		this.addChild(this.arr, 100);
		
		this.tipItem = new ButtonScale(this,"#button/tip.png",this.eventMenuCallback);
		this.tipItem.setTag(TAG_TIP);
		this.backItem = new ButtonScale(this,"#go_back.png",this.eventMenuCallback);
		this.backItem.setTag(TAG_CLOSE);
		
		this.tipItem.setPosition(40 + this.tipItem.width * 0.5, 40 + this.tipItem.height * 0.5);
		this.backItem.setPosition(40 + this.backItem.width * 0.5, gg.height - this.backItem.height * 0.5 - 40);
		
		this.tip_frame = new WinFrame(this);
		this.tip = new Tip("", gg.width * 0.75);
		this.flash = new Flash("", gg.width * 0.6);
		
		this.time = new cc.LabelTTF("", gg.fontName, gg.fontSize);
		this.time.setAnchorPoint(0, 1);
		this.time.setPosition(gg.width - 200, gg.height - 40);
		this.addChild(this.time, 12);
		
		this.score = new cc.LabelTTF("分数：0", gg.fontName, gg.fontSize);
		this.score.setAnchorPoint(0, 1);
		$.down(this.time, this.score, 10);
		this.addChild(this.score, 12);
		
		this.tip.setPosition(gg.width * 0.2, 10);
		this.flash.setPosition(300, 620);

		this.addChild(this.tip, 10);
		this.addChild(this.flash, 10);

		if(gg.teach_type == TAG_LEAD){
			this.score.setVisible(false);
		} else {
			this.tip.setVisible(false);
		}
		this.updateTime();
		this.schedule(this.updateTime, 1);
		
		this.rotateAction = cc.repeatForever(cc.rotateBy(2, 360));
		this.rotateAction.retain();// jsb BUG
		
		gg.tip_layer = this;
	},
	sayFlash:function(str){
		this.flash.doFlash(str);
	},
	hiddleTip:function(){
		this.tipItem.setVisible(false);
	},
	showTip:function(){
		this.tipItem.setVisible(true);
	},
	updateTime:function(){
		var now = new Date();
		var ds = Math.round((now - gg.begin_time) / 1000);
		this.time.setString("计时：" + ds + "秒");
	},
	mdScore:function(score){
		if(!score){
			return;
		}
		gg.score += score;
		if(gg.score <= 0){
			gg.score = 0;
		}
		this.updateScore();
	},
	updateScore:function(){
		this.score.setString("分数：" + gg.score);
	},
	over:function(){
		this.unschedule(this.updateTime);
		gg.end_time = new Date();
	},
	eventMenuCallback: function(pSender) {
		switch (pSender.getTag()){
		case TAG_TIP:
			if(this.tip_frame.isOpen()){
				cc.log("关闭提示");
				gg.synch_l = false;
				this.tip_frame.close();
			} else {
				cc.log("打开提示");
				gg.synch_l = true;
				this.tip_frame.open();
				ll.tip.mdScore(-3);
			}
			break;
		case TAG_CLOSE:
			$.runScene(new StartScene());
			break;
		default:
			break;
		}
	}
});