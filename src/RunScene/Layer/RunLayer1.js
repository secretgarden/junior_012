var RunLayer1 = BaseLayer.extend({
	arr:null,
	scene:null,
	ctor:function (parent) {
		this._super(parent);
		this.init();
	},
	init:/**
			 * 初始化,
			 */
	function () {
		this._super();
		cc.log("RunLayer1初始化");
		this.loadMain();
		this.loadTipFrame();
	},
	loadMain:/**
				 * 加载主图
				 */
	function(){
		this.body = new cc.Sprite("#body.png");
		this.body.setPosition(gg.c_p);
		this.body.setScale(0.7);
		this.addChild(this.body);
		var bw = this.body.width;
		var bh = this.body.height;
		for(var i in frameInfo){
			var info = frameInfo[i];
			info.finish = false;
			var bt = new Button(this.body,10,info.tag,info.img,this.callback,this);
			bt.setPosition(cc.p(info.px, bh - info.py));
			bt.setAnchorPoint(0, 1);
			bt.setOpacity(1);
			bt.info = info;
		}
	},
	loadTipFrame:function(){
		this.labelNode = new cc.Node();
		this.addChild(this.labelNode);
		var last = null;
		for(var i in frameInfo){
			var info = frameInfo[i];
			var label = new Label(this.labelNode, info.name);
			label.setTag(info.tag);
			label.setAnchorPoint(0, 0.5);
			if(!!last){
				label.down(last, 10);
			} else {
				label.setPosition(250, 620);
			}
			last = label;
		}
	},
	checkOver:/**
				 * 是否通关
				 * 
				 * @returns {Boolean}
				 */
	function(){
		for(var i in frameInfo){
			var info = frameInfo[i];
			if(!info.finish){
				return false;
			}
		}
		return true;
	},
	curSel:null,
	callback:function (p){
		if(!!this.curSel){
			this.stop(this.curSel);
		}
		this.curSel = p;
		this.flash(this.curSel);
		
		if(!p.info.finish){
			// 操作成功
			ll.tip.mdScore(10);
			var label = this.labelNode.getChildByTag(p.info.tag);
			var sure = new cc.Sprite("#ui/sel_rel.png");
			sure.setAnchorPoint(1, -0.2);
			label.addChild(sure);
			p.info.finish = true;
		} else {
			ll.tip.mdScore(-3);
		}
		p.setBottom();
		_.clever();
		if(this.checkOver()){
			gg.flow.next();
		}
		new ArrowShowTip(this).init(p.info.st).setPosition(850, gg.c_height);
	},
	flash:function (bt){
		var fade1 = cc.fadeTo(0.3, 50);
		var fade2 = cc.fadeTo(0.3, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		bt.runAction(flash);
	},
	stop:function (bt){
		bt.stopAllActions();
		bt.setOpacity(1);
	},
	destroy:/**
			 * 销毁
			 */
	function(){
		this._super();
	},
	onExit:function(){
		this._super();
	}
});
                      
frameInfo = [
    {name:"大肠",tag:TAG_DACHANG,px:375,py:660,img:"#dachang.png",st:"大肠:吸收食物残渣中的水分"},
    {name:"肝脏",tag:TAG_GANZANG,px:401,py:561,img:"#ganzang.png",st:"肝脏:分泌胆汁,\n帮助消化,还能解毒"},
    {name:"口腔",tag:TAG_KOUQIANG,px:382,py:186,img:"#kouqiang.png",st:"口腔:牙齿将食物磨碎,\n舌品尝不同的味道"},
    {name:"阑尾",tag:TAG_LANWEI,px:404,py:888,img:"#lanwei.png",st:"阑尾"},
    {name:"食道",tag:TAG_SHIDAO,px:512,py:269,img:"#shidao.png",st:"食道:把食物向下推进入胃"},
    {name:"唾液腺",tag:TAG_TUOYEXIAN,px:423,py:211,img:"#tuoyexian.png",st:"唾液腺:分泌唾液,\n帮助消化和吞咽食物"},
    {name:"胃",tag:TAG_WEI,px:528,py:575,img:"#wei.png",st:"胃:暂时存储食物,\n进行初步消化"},
    {name:"小肠",tag:TAG_XIAOCHANG,px:423,py:724,img:"#xiaochang.png",st:"小肠:消化食物和吸收营养物质"},
    {name:"咽",tag:TAG_YAN,px:479,py:235,img:"#yan.png",st:"咽:将食物送入食管"},
    {name:"胰腺",tag:TAG_YIXIAN,px:526,py:662,img:"#yixian.png",st:"胰腺:分泌消化液"},
    {name:"直肠",tag:TAG_ZHICHANG,px:490,py:891,img:"#zhichang.png",st:"直肠:大肠的末端"},
    {name:"肛门",tag:TAG_GANGMEN,px:482,py:964,img:"#gangmen.png",st:"肛门:把粪便排出体外"}];

