var StartBackgroundLayer = cc.LayerColor.extend({
	zindex:5,
    ctor:function () {
        this._super();
        this.loadBg();
// this.loadTitle();
        this.loadLogo();
// this.loadLogo1();
        return true;
    },
    loadBg : function(){
    	this.setColor(cc.color(111,111,111));
    	var node = new cc.Sprite(res_start.start_back);
        this.addChild(node);
        node.setPosition(gg.c_p);
    },
    loadTitle:function(){
    	var title = new cc.Sprite("#title.png");
    	title.setPosition(title.width * 0.5, gg.height - title.height * 0.5);
    	this.addChild(title,10);
    },
    loadLogo:/**
				 * 学校logo
				 */
    function(){
    	var logo = new cc.Sprite(res_start.logo1);
    	logo.setAnchorPoint(0, 1);
    	logo.setPosition(40, gg.height - 40);
    	this.addChild(logo, 10);
    },
    loadLogo1:/**
				 * 沃赛logo
				 */
    function(){
    	var logo1 = new cc.Sprite(res_start.logo1);
    	logo1.setAnchorPoint(1, 1);
    	logo1.setPosition(gg.width - 40, gg.height - 40);
    	this.addChild(logo1, 10);
    }
});