TAG_YINDAO = 1001;
TAG_SHIZHAN = 1002;
TAG_QUWEI = 1003;
TAG_QITA = 1004;
TAG_ABOUT = 1401;
TAG_TEST = 1402;
TAG_LIANX = 1403;
TAG_YUANL = 1404;
TAG_MUDE = 1405;
TAG_DTL = 1406;
TAG_PLATFORM = 1407;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        // 加载菜单栏按钮
        this.loadButton();
        // 返回平台
        this.loadPlatform();
        // 加载标题
        this.loadTitle();
        return true;
    },
    loadTitle: function(){
    	var title = new cc.Sprite("#title.png");
    	title.setPosition(gg.c_width, gg.height - 194 - title.height * 0.5);
    	this.addChild(title);
    },
    loadButton:function(){
    	var yinD = new ButtonScale(this,"#YinD.png",this.callback);
    	yinD.setLocalZOrder(5);
    	yinD.setPosition(398 + 200 * 0.5, 80 + 62 + 57 + 68 * 0.5);
    	yinD.setTag(TAG_YINDAO);
    	
    	var Shiz = new ButtonScale(this,"#Shiz.png",this.callback);
    	Shiz.setLocalZOrder(5);
    	Shiz.setPosition(398 + 200 * 1.5 + 84, 80 + 62 + 57 + 68 * 0.5);
    	Shiz.setTag(TAG_SHIZHAN);
    	
    	var MuDe = new ButtonScale(this,"#MuDe.png",this.callback);
    	MuDe.setPosition(240 + 192 * 0.5, 80 + 62 * 0.5);
    	MuDe.setLocalZOrder(5);
    	MuDe.setTag(TAG_MUDE);
    	
    	var YuanL = new ButtonScale(this,"#YuanL.png",this.callback);
    	YuanL.setPosition(240 + 192 * 1.5 + 118, 80 + 62 * 0.5);
    	YuanL.setLocalZOrder(5);
    	YuanL.setTag(TAG_YUANL);
    	
    	var Quw = new ButtonScale(this,"#Quw.png",this.callback);
    	Quw.setPosition(240 + 192 * 2.5 + 118 * 2, 80 + 62 * 0.5);
    	Quw.setLocalZOrder(5);
    	Quw.setTag(TAG_QUWEI);

    },
    cWin:function(name){
    	if(name == null || name == ""){
    		this.win.runAction(cc.fadeOut(0.15));	
    		return;
    	}
    	var seq = cc.sequence(cc.fadeOut(0.15),cc.callFunc(function(){
    		this.win.setSpriteFrame(name);
    	}, this),cc.fadeIn(0.15));
    	this.win.runAction(seq);
    },
    loadPlatform : function(){
    	var platform = new ButtonScale(this,"#platform.png", this.callback);
		platform.setLocalZOrder(5);
		platform.setPosition(140,30);
		platform.setTag(TAG_PLATFORM);
    },
    callback:function(pSend){
    	switch(pSend.getTag()){
    		case TAG_PLATFORM:
    			cc.log("结束");
    			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
    				history.go(-1);
    			} else if(cc.sys.os == "IOS"){
    			} else if(cc.sys.os == "Android"
    				|| cc.sys.os == "Windows"){
    				cc.director.end();		
    			}
    			break;
	    	case TAG_YINDAO:
	    		cc.log("进入引导模式");
	    		gg.teach_type = TAG_LEAD;
	    		this.runNext(pSend.getTag());
	    		break;
	    	case TAG_SHIZHAN:
	    		cc.log("进入实战模式");
	    		gg.teach_type = TAG_REAL;
	    		this.runNext(pSend.getTag());
	    		break;
	    	case TAG_QUWEI:
	    		cc.log("进入小游戏模式");
	    		this.runNext(pSend.getTag());
	    		break;
	    	case TAG_ABOUT:
	    		this.runNext(pSend.getTag());
	    		break;
	    	case TAG_TEST:
	    		this.cWin("");
	    		this.runNext(pSend.getTag());
	    		break;
	    	case TAG_LIANX:
	    		this.cWin("");
	    		break;
	    	case TAG_YUANL:
// this.cWin("axiom.png");
	    		break;
	    	case TAG_MUDE:
// this.cWin("target.png");
	    		break;
    	}
    },
    runNext:function(tag){
    	switch(tag){
	    	case TAG_YINDAO:
	    	case TAG_SHIZHAN:
	    		if(gg.run_load){
	    			$.runScene(new RunScene());
	    			return;
	    		}
	    		break;
	    	case TAG_QUWEI:
	    		if(gg.game_load){
	    			 $.runScene(new GameScene());
	    			return;
	    		}
	    		break;
	    	case TAG_TEST:
	    		if(gg.game_load && gg.test_load){
	    			$.runScene(new TestScene());
	    			return;
	    		}
	    		break;
	    	case TAG_ABOUT:
	    		if(gg.about_load){
	    			$.runScene(new AboutScene());
	    			return;
	    		}
	    		break;
    	}
    	if(this.tip == null){
    		this.tip = new cc.LabelTTF("加载中，请稍候再试",gg.fontName,gg.fontSize);
    		this.tip.setColor(cc.color(0,0,0,0));
    		this.tip.setPosition(gg.width * 0.5, gg.height * 0.5)
    		this.addChild(this.tip);
    	} else {
    		this.tip.setVisible(true);
    	}
    	this.scheduleOnce(function(){
    		this.tip.setVisible(false);	
    	}.bind(this), 1.5);
    }
});
