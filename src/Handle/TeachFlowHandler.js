var /**
	 * 仿真流程管理
	 * 
	 * 
	 */
TeachFlow = cc.Class.extend({
	step:0,
	flow:null,
	main:null,
	over_flag: false,
	curSprite:null,
	ctor: function(){
		this.init();
	},
	setMain:function(main){
		this.main=main;
	},
	init: function(){
		var no = 0;
		for(var i in teachFlow){
			teachFlow[i].finish = false;
			teachFlow[i].cur = false;
			teachFlow[i].id = i - -1;
			if(teachFlow[i].action == null){
				teachFlow[i].action = ACTION_NONE;
			}
			if(teachFlow[i].node){
				teachFlow[i].nodeNo = ++no; 
			} else {
				teachFlow[i].nodeNo = no
			}
		}
	},
	start:/**
			 * 开始流程
			 */
	function(step){
		this.over_flag = false;
		if(step == null){
			this.step = 0;
		} else {
			this.step = step;
		}
		for(var i in teachFlow){
			teachFlow[i].finish = false;
		}
		this.flow = null;
		/* 新标准，开始的时候，执行下一步 */
		this.next(true);
	},
	next:/**
			 * 执行下一步操作， 定位当前任务
			 */
	function(){
		if(this.over_flag){
			return;
		}
		if(this.curSprite!=null){
			this.curSprite.setEnable(false);
			this.curSprite = null;
		}
		if(this.flow != null){
			this.flow.cur = false;
			// 标记任务已完成
			this.flow.finish = true;
		}
		this.flow = teachFlow[this.step++];
		if(this.flow.node){
			// 开始一个新节点
			ll.main.loadRun();
		}
		if(this.flow.finish){
			// 如果任务已完成，跳过当前步骤
			this.next();
		}
		this.refresh();
	},
	refresh:/**
			 * 刷新当前任务状态，设置闪现，点击等状态
			 */
	function(){
		// 刷新提示
		this.flow.cur = true;
		if(this.flow.tip != null){
			ll.tip.tip.doTip(this.flow.id + "." + this.flow.tip);
		}
		if(this.flow.flash != null){
			ll.tip.flash.doFlash(this.flow.flash);
		}
		if(this.step > teachFlow.length - 1){
			this.over();
		}
		this.initCurSprite();
		if(this.curSprite!=null){
			this.location();
			this.curSprite.setEnable(true);
		}
	},
//	location:/**
//				 * 定位箭头
//				 */
//	function(){
//		ll.tip.arr.pos(this.curSprite);		
//	},
	location:/**
	 * 定位箭头
	 */
		function(){
		var tag = gg.flow.flow.tag;
		if(tag instanceof Array){
			if(TAG_LIB_MIN < tag[1]){
				if(ll.run.lib.isOpen()){
					ll.tip.arr.pos(this.curSprite);
				}else{
					ll.tip.arr.setPosition(gg.width-35,460);
				}
			}else{
				ll.tip.arr.pos(this.curSprite);
			}
		}
		else {
			ll.tip.arr.pos(this.curSprite);
		}		
	},
	getStep:/**
			 * 获取当前步数
			 * 
			 * @returns {Number}
			 */
	function(){
		return this.step;
	},
	initCurSprite:/**
					 * 遍历获取当前任务的操作对象
					 */
	function(){
		var tag = this.flow.tag;
		if(tag == null || tag == undefined){
			return;
		}
		var root = ll.run;
		var sprite = null;
		if(tag == TAG_BUTTON_LIB){
			sprite = ll.tool.getChildByTag(tag);
		} else if(tag instanceof Array){
			// 数组
			for (var i in tag) {
				root = root.getChildByTag(tag[i]);
			}
			sprite = root;
		} else {
			// 单个tag
			var sprite = root.getChildByTag(tag);
		}
		if(sprite != null){
			this.curSprite = sprite;
			return;
		}
	},
	checkTag:/**
				 * 检查是否当前步骤
				 * 
				 * @deprecated 使用新Angel类，不再判断是否当前步骤
				 * @param tag
				 * @returns {Boolean}
				 */
		function(tag){
		var cur_flow = teachFlow[this.step - 1];
		if(cur_flow.tag == tag){
			return true;
		} else {
			return false;
		}
	},
	over:/**
			 * 流程结束，计算分数，跳转到结束场景，
			 */
		function(){
		this.over_flag = true;
		this.flow = over;
		gg.lastStep = this.step;
		this.main.over();
	}
});

// 任务流

over = {tip:"恭喜过关"};



