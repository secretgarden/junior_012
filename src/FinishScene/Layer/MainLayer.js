var FinishMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        this.loadTip();
        return true;
    },
    loadTip: function(){
    	this.tip = new cc.LabelTTF("恭喜过关", gg.fontName, 40);
    	this.tip.setPosition(gg.c_width,gg.height);
    	this.addChild(this.tip, 10);

    	var jump =	cc.jumpTo(3,cc.p(gg.c_width, 450), 80, 6)
    	var jump2 = cc.jumpTo(2,cc.p(gg.c_width, 450), 80, 3);
    	var seq = cc.sequence(jump, jump2);
    	this.tip.runAction(seq);
    	_.over();
    	this.scheduleOnce(function(){
    		this.loadStartButton();
    		this.loadStartButtonParticle();
    	},3);
    },
    loadStartButton : function(){
        this.restart = new ButtonScale(this,"#restart.png",this.callback);
        this.restart.setPosition(gg.width * 0.3, gg.height * 0.4);
        
        this.platform = new ButtonScale(this,"#back_platform.png",this.callback);
        this.platform.setPosition(gg.width * 0.7, gg.height * 0.4);
    },
    loadStartButtonParticle : function(){
    	var restart_node = new cc.ParticleSystem(res_start.follow_p);
    	this.addChild(restart_node);
    	restart_node.setPosition(this.restart.x - this.restart.width / 2, this.restart.y - this.restart.height / 2);
    	var action = this.getFollowAction(10, this.restart);
    	restart_node.runAction(action);
    },
    getFollowAction : function(stretch, node){
        var width = node.width;
        var height = node.height;

        var bezier1 = [cc.p(-stretch, 0), cc.p(-stretch, height), cc.p(0, height)];
        var bezierBy1 = cc.bezierBy(0.6, bezier1);
        var move1 = new cc.moveBy(0.7, cc.p(width, 0));

        var bezier2 = [cc.p(stretch, 0), cc.p(stretch, -height), cc.p(0, -height)];
        var bezierBy2 = cc.bezierBy(0.6, bezier2);
        var move2 = new cc.moveBy(0.7, cc.p(-width, 0));

        var action = cc.sequence(bezierBy1, move1, bezierBy2, move2).repeatForever();
        return action;
    },
    callback:function(pSend){
    	switch(pSend){
	    	case this.restart:
	    		cc.log("重新开始");
	    		_.stop();
	    		$.runScene(new StartScene());
	    		break;
	    	case this.platform:
	    		_.stop();
	    		cc.log("返回大厅");
	    		if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
	    			history.go(-1);
	    		} else if(cc.sys.os == "IOS"){
	    		} else if(cc.sys.os == "Android"
	    			|| cc.sys.os == "Windows"){
	    			cc.director.end();		
	    		}
	    		break;
    	}
    }
});
