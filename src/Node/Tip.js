Tip = cc.LabelTTF.extend({
	fSize:0,
	d_width:null,
	ctor:function (text, width, fontName,fontSize) {
		this.d_width = width;
		if(fontName == null){
			fontName = gg.fontName;
		}
		if(fontSize == null){
			this.fSize = gg.fontSize;
		} else {
			this.fSize = fontSize;
		}
		text = $.format(text, this.d_width, this.fSize);
		this._super(text, fontName, this.fSize);
		this.setAnchorPoint(0, 0);
		this.setColor(cc.color(255, 255, 255));
	},
	doTip:/**
			 * 更改提示
			 * 
			 * @param text
			 */
	function(text){
		text = $.format(text, this.d_width, this.fSize);
		this.setString(text);
	}
});

TipFrame = cc.LayerColor.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	tipLayer:null,
	cell:null,
	ctor:function (tipLayer) {
		this._super();
		this.setColor(cc.color(255, 255, 255, 191));
		this.setOpacity(191);
		this.tipLayer = tipLayer;
		this.init();
		qq = this;
	},
	init:function (){
		this.node = new cc.Node();
		this.addChild(this.node);
		this.cur = 0;
		this.posX = 5;
		this.posY = gg.height;
		for(var i = 0;i< teachFlow.length; i++){
			var pre = i - -1 + ".";
			var text = $.format(pre + teachFlow[i].tip, 0.5 * gg.width, gg.fontSize);
			var tip = new cc.LabelTTF(text, gg.fontName, gg.fontSize);
			tip.setAnchorPoint(0, 0);
			this.posY = this.posY - tip.height - this.lineMarign;
			tip.setPosition(this.posX, this.posY);
			this.tip[i] = tip;
			this.node.addChild(tip);
			if(this.cell == null){
				this.cell = tip.height + this.lineMarign;
			}
		}
	},
	open:function (){
		var move = cc.moveTo(0.5,cc.p(gg.width * 0.5, 0));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		this.open_flag = true;
	},
	close:function (){
		var move = cc.moveTo(0.5,cc.p(gg.width, 0));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		this.open_flag = false;
	},
	isOpen:function (){
		return this.open_flag;
	},
	up:function (){
		if(this.cur <= 0){
			return;
		}
		var curTip = this.tip[--this.cur];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, - curTip.height - this.lineMarign)));
	},
	down:function (){
		if(this.cur >= this.tip.length - 1){
			return;
		}
		var curTip = this.tip[this.cur++];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, curTip.height + this.lineMarign)));
	},
	local:function(step){
		var last = this.cur;
		this.cur = step - 1;
		if(this.cur < 0){
			this.cur = 0;
		}
		var result = 0;
		if(this.cur == last){
			return;
		} else if(this.cur > last){
			for(var i = last; i < this.cur; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
		} else {
			for(var i = this.cur; i < last; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
			result = -result;
		}
		var pos = this.node.getPosition();
		this.node.setPosition(pos.x,pos.y + result);
	},
	refresh:function (){
		for(var i=0;i<this.tip.length;i++){
			if(teachFlow[i].cur){
				this.tip[i].setColor(cc.color(1,142,70));	
			} else {
				this.tip[i].setColor(cc.color(0,0,0,0));
			}
		}	
	},
	loadSlide:/**
				 * 滑动
				 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:UpperLowerSliding.onTouchBegan,
			onTouchMoved:UpperLowerSliding.onTouchMoved,
			onTouchEnded:UpperLowerSliding.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	}
});