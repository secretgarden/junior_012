var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
// this.setEnable(false);
	},
	preCall:function(){
		// 隐藏箭头
		ll.tip.arr.out();
		// 删除提示
		if(!!ll.run.st){
			ll.run.st.kill();			
		}
	},
	exeUnEnable:function(){
		// 操作失败
	},
	kill:function(){
		this.removeFromParent(true);
	},
	hiddleAndKill:/**
					 * @param dt
					 *            时间,默认0.5秒
					 */
	function(dt){
		if(dt == null){
			dt = 0.5;
		}
		this.runAction(cc.fadeOut(dt),cc.callFunc(function(){
			this.kill();
		}, this));
	}
})