WinFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	cell:null,
	ctor:function (parent) {
		this._super();
		parent.addChild(this);
		this.setPosition(0, gg.height);
		this.init();
	},
	init:function (){
		var frame = new cc.Sprite("#tip_frame.png");
		frame.setPosition(gg.c_p);
		this.addChild(frame);
		
		var close = new ButtonScale(this,"#tip_frame_close.png",this.callback);
		close.setPosition(1135, 655);
	},
	open:function (){
		this.stopAllActions();
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		this.stopAllActions();
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	isOpen:function (){
		return this.open_flag;
	},
	callback:function(){
		if(this.isOpen()){
			cc.log("关闭提示");
			gg.synch_l = false;
			this.close();
		}
	}
});