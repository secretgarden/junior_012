//start
var res_start = {
		start_g : "res/start.png",
		start_p : "res/start.plist",
		show_tip : "res/show_tip.png",
		start_back : "res/start_back.jpg",
		logo : "res/logo.png",
		logo1 : "res/logo1.png",
		bg_mp3 : "res/music/bg.mp3"
};

var g_resources_start = [];
for (var i in res_start) {
	g_resources_start.push(res_start[i]);
}

// run
var res_run = {
		run_p : "res/run.plist",
		run_g : "res/run.png",
		run_back : "res/run_back.jpg",
		show_tip_bg : "res/show_tip_bg.png",
		sel_p : "res/sel.plist",
		sel_g : "res/sel.png",
		ok_mp3 : "res/music/ok.mp3",
		error_mp3 : "res/music/error.wav",
		clock_mp3 : "res/music/clock.wav"
};

var g_resources_run = [];
for (var i in res_run) {
	g_resources_run.push(res_run[i]);
}

// game
var res_game = {
		game_g : "res/game.png",
		game_p : "res/game.plist"
};

var g_resources_game = [];
for (var i in res_game) {
	g_resources_game.push(res_game[i]);
}

// test
var res_test = {
		subject : "res/JsonLib/subject.json",
		test_g : "res/test.png",
		test_p : "res/test.plist"
};

var g_resources_test = [];
for (var i in res_test) {
	g_resources_test.push(res_test[i]);
}

// finish
var res_finish = {
		over_mp3 : "res/music/over.mp3",
		firework_g : "res/Duang/firework.png",
		firework_p : "res/Duang/firework.plist"
};

var g_resources_finish = [];
for (var i in res_finish) {
	g_resources_finish.push(res_finish[i]);
}

// about
var res_about = {
		about_j : "res/JsonLib/about.json",
		about_g : "res/about.png",
		about_p : "res/about.plist"
};

var g_resources_about = [];
for (var i in res_about) {
	g_resources_about.push(res_about[i]);
}